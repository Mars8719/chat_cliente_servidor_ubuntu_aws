import socket
import threading
import mysql.connector

HOST = '18.231.34.221'
PORT = 1234
LISTENER_LIMIT = 5
active_clients = []

# Crear una conexión a la base de datos MySQL
db_connection = mysql.connector.connect(
    host="18.231.34.221",
    user="xander",
    password="JASLca2004!",
    database="chat_db"
)
cursor = db_connection.cursor()

def listen_for_messages(client, username):

    while 1:
        message = client.recv(2048).decode('utf-8')
        if message != '':
            final_msg = username + '~' + message
            send_messages_to_all(final_msg)
            save_message_to_db(username, message)

        else:
            print(f"The message sent from client {username} is empty")

def send_message_to_client(client, message):
    client.sendall(message.encode())

def send_messages_to_all(message):
    for user in active_clients:
        send_message_to_client(user[1], message)

def save_message_to_db(username, message):
    # Guardar el mensaje y la hora en la base de datos
    cursor.execute("INSERT INTO chat (username, message, timestamp) VALUES (%s, %s, NOW())", (username, message))
    db_connection.commit()

def client_handler(client):
    while 1:
        username = client.recv(2048).decode('utf-8')
        if username != '':
            active_clients.append((username, client))
            prompt_message = "SERVER~" + f"{username} added to the chat"
            send_messages_to_all(prompt_message)
            break
        else:
            print("Client username is empty")

    threading.Thread(target=listen_for_messages, args=(client, username,)).start()

def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        server.bind((HOST, PORT))
        print(f"Running the server on {HOST} {PORT}")
    except:
        print(f"Unable to bind to host {HOST} and port {PORT}")

    server.listen(LISTENER_LIMIT)

    while 1:
        client, address = server.accept()
        print(f"Successfully connected to client {address[0]} {address[1]}")
        threading.Thread(target=client_handler, args=(client,)).start()

if __name__ == '__main__':
    main()
